/** @jsx React.DOM */

function isValidDate(date){
    var matches = /^(\d{4})[-\/](\d{2})[-\/](\d{2})$/.exec(date);
    if (matches == null) return false;
    var d = matches[3];
    var m = matches[2] - 1;
    var y = matches[1];
    var composedDate = new Date(y, m, d);
    return composedDate.getDate() == d &&
            composedDate.getMonth() == m &&
            composedDate.getFullYear() == y;
}

var ItemFieldInput = React.createClass({
    getInitialState: function(){
        return {value: this.props.value || ""};
    },
    componentDidMount: function(){
        if (this.props.type == "date"){
            var node = this.getDOMNode();
            var $datepicker = $(node).datepicker({
                dateFormat: "yy-mm-dd",
                onSelect: function(date){
                    this.setValue(date);
                    node.focus();
                }.bind(this)
            });
            if (this.props.focus){
                $datepicker.focus();
            }
        }
    },
    handleChange: function(event){
        this.setValue(event.target.value);

    },
    getValue: function(){
        return this.state.value;
    },
    setValue: function(value){
        this.setState({value: value}, function callback(){
              if (this.props.onChange)
                  this.props.onChange();
          });
    },
    isValid: function(){
        if (this.props.type == "char")
            return this.state.value != "";
        if (this.props.type == "int")
            return /^\-?\d+$/.test(this.state.value);
        if (this.props.type == "date")
            return isValidDate(this.state.value);
    },
    render: function(){
        return (<input type={this.props.type == "int"? "number" : "text"}
                       className={(this.isValid() || this.state.value == "")? "input-field" : "input-field-error"}
                       name={this.props.id}
                       id={this.props.inputId}
                       value={this.state.value}
                       autoFocus={this.props.focus? "autofocus" : ""}
                       onChange={this.handleChange}
                />);
    }
});

var AddItemForm = React.createClass({
    getInitialState: function(){
        return {valid: false};
    },
    handleSubmit: function(event){
        var item = {},
            valid = true;
        this.props.fields.forEach(function(field){
            var input = this.refs[field.id];
            item[field.id] = input.getValue();
            valid = valid && input.isValid();
        }.bind(this));
        if (!valid)
            return false;
        $.ajax({
            url: this.props.modelName + '/',
            method: 'post',
            dataType: 'json',
            data: item,
            success: function(data){
                this.props.fields.forEach(function(field){
                    this.refs[field.id].setValue('');
                }.bind(this));
                this.props.onAdd(data);
            }.bind(this),
            error: function(xhr, status, err){
                Object.keys(xhr.responseJSON).forEach(function(field){
                    alert(field + ": " + xhr.responseJSON[field]);
                });
            }.bind(this)
        });
        return false;
    },
    handleFieldChange: function(){
        var valid = true;
        this.props.fields.forEach(function(field){
            var input = this.refs[field.id];
            valid = valid && input.isValid();
        }.bind(this));
        this.setState({valid: valid});
    },
    isValid: function(){
        for(var i = 0; i < this.props.fields.length; ++i){
            var field = this.props.fields[i],
                input = this.refs[field.id];
            console.log(this.refs);
            if (!input || !input.isValid())
                return false;
        }
        return true;
    },
    render: function(){
        var fields = this.props.fields.map(function(field){
            var inputId = "form-field-" + field.id;
            return (<p key={this.props.modelName + "-" + field.id}>
                <label htmlFor={inputId}>{field.title}</label>
                <ItemFieldInput id={field.id}
                                type={field.type}
                                inputId={inputId}
                                onChange={this.handleFieldChange}
                                ref={field.id}
                />
            </p>);
        }.bind(this));
        return (<form action={this.props.modelName + "/"} onSubmit={this.handleSubmit} method="post">
            <fieldset>
                <legend>{this.props.title}: добавить</legend>
                {fields}
                <input type="submit" value="Добавить" disabled={this.state.valid? "" : "disabled"} />
            </fieldset>
        </form>);
    }
});

var ModelTableCell = React.createClass({
    getInitialState: function(){
        return {editable: false};
    },
    makeEditable: function(){
        this.setState({editable: true});
    },
    makeUneditable: function(){
        this.setState({editable: false});
    },
    saveNewValue: function(){
        if (!this.refs.field.isValid()){
            return;
        }
        var change = {};
        change[this.props.field.id] = this.refs.field.getValue();
        $.ajax({
            url: this.props.modelName + '/' + this.props.itemId + '/',
            method: 'put',
            dataType: 'json',
            data: change,
            success: function(data){
                this.props.onEdit(data);
                this.makeUneditable();
            }.bind(this),
            error: function(xhr, status, err){
                alert(xhr.responseJSON[this.props.field.id]);
            }.bind(this)
        });
    },
    handleKeyPress: function(event){
        if (event.keyCode == 13) {
            // Enter
            this.saveNewValue();
        } else if (event.keyCode == 27){
            // ESC
            this.makeUneditable();
        }
    },
    render: function(){
        if (!this.state.editable){
            return (<td onClick={this.makeEditable}>{this.props.value}</td>);
        } else {
            return (<td onKeyDown={this.handleKeyPress}>
                <ItemFieldInput id={this.props.field.id}
                                type={this.props.field.type}
                                inputId={"table-field-" + this.props.field.id}
                                value={this.props.value}
                                focus={true}
                                ref="field"
                />
            </td>);
        }
    }
});

var ModelTableRow = React.createClass({
    render: function(){
        var cells = [<td key={this.props.modelName + '-' + this.props.item.id + '-id'}>{this.props.item.id}</td>];
        this.props.fields.forEach(function(field){
            var value = this.props.item[field.id];
            cells.push(<ModelTableCell value={value}
                                       field={field}
                                       itemId={this.props.item.id}
                                       modelName={this.props.modelName}
                                       onEdit={this.props.onEdit}
                                       key={this.props.modelName + '-' + this.props.item.id + '-' + field.id}
                       />);
        }.bind(this));
        return (<tr>
            {cells}
        </tr>);
    }
});

var ModelTable = React.createClass({
    render: function(){
        var rows = this.props.items.map(function(item){
            return (<ModelTableRow fields={this.props.fields}
                                   item={item}
                                   modelName={this.props.modelName}
                                   onEdit={this.props.onEdit}
                                   key={this.props.modelName + '-' + item.id}
                                   />);
        }.bind(this));
        var headerColumns = [<td key={this.props.modelName + "-id"}>id</td>];
        this.props.fields.forEach(function(field){
            headerColumns.push(<td key={this.props.modelName + "-" + field.id}>{field.title}</td>);
        }.bind(this));
        return (<table>
            <thead><tr>{headerColumns}</tr></thead>
            <tbody>{rows}</tbody>
        </table>);
    }
});

var ModelInfo = React.createClass({
    getInitialState: function(){
        return {items: []}
    },
    componentDidMount: function(){
        $.ajax({
            url: this.props.modelName + '/',
            dataType: 'json',
            success: function(data){
                this.setState({items: data});
            }.bind(this),
            error: function(xhr, status, err){
                console.error(this.props.modelName, status, err.toString());
            }.bind(this)
        });
    },
    handleItemAdd: function(item){
        var items = this.state.items;
        items.push(item);
        this.setState({items: items});
    },
    handleItemEdit: function(changedItem){
        var items = this.state.items.map(function(item){
            if (item.id == changedItem.id)
                return changedItem;
            return item;
        });
        this.setState({items: items});
    },
    render: function(){
        return (<div className="model-info">
            <div className="model-table">
                <ModelTable modelName={this.props.modelName}
                            fields={this.props.fields}
                            items={this.state.items}
                            onEdit={this.handleItemEdit}
                />
            </div>
            <div className="add-model-form">
                <AddItemForm modelName={this.props.modelName}
                             title={this.props.title}
                             fields={this.props.fields}
                             onAdd={this.handleItemAdd}
                />
            </div>
        </div>);
    }
});

var ModelSelectorItem = React.createClass({
    handleSelection: function(){
        this.props.onModelSelection(this.props.name);
        return false;
    },
    render: function(){
        if (this.props.selected){
            return (<li><b>{this.props.model.title}</b></li>);
        }
        return (<li>
            <a href="#" onClick={this.handleSelection}>
                {this.props.model.title}
            </a>
        </li>)
    }
});

var ModelSelector = React.createClass({
    render: function(){
        var modelList = Object.keys(this.props.models).map(function(modelName){
            var model = this.props.models[modelName],
                selected = this.props.selectedModelName == modelName;
            return (<ModelSelectorItem model={model}
                                       name={modelName}
                                       selected={selected}
                                       onModelSelection={this.props.onModelSelection}
                                       key={modelName}
                    />);
        }.bind(this));
        return (<div className="model-selector">
                        <ul>
                            {modelList}
                        </ul>
                </div>);
    }
});

var Application = React.createClass({
    getInitialState: function(){
        return {
            selectedModelName: Object.keys(this.props.models)[0]
        };
    },
    handleModelSelection: function(modelName){
        this.setState({
            selectedModelName: modelName
        })
    },
    render: function(){
        var selectedModel = this.props.models[this.state.selectedModelName];
        return (<div>
            <ModelSelector models={this.props.models}
                           selectedModelName={this.state.selectedModelName}
                           onModelSelection={this.handleModelSelection}
            />
            <ModelInfo modelName={this.state.selectedModelName}
                       fields={selectedModel.fields}
                       title={selectedModel.title}
                       key={this.state.selectedModelName}
            />
        </div>);
    }
});


React.renderComponent(<Application models={MODELS} />, document.body);