import yaml
from django.db import models
from django.conf import settings


def parse_models(stream):
    return yaml.load(stream)


def get_parsed_models():
    with open(settings.YAML_MODELS) as f:
        return parse_models(f)


def get_field(field_type, title):
    if field_type == 'int':
        return models.IntegerField(null=True, verbose_name=title)
    if field_type == 'char':
        return models.TextField(null=True, verbose_name=title)
    if field_type == 'date':
        return models.DateField(null=True, verbose_name=title)
    raise ValueError('Unexpected field_type')


def create_model(name, title, fields):
    class Meta:
        verbose_name = title
        verbose_name_plural = title
        app_label = 'main'

    attrs = {
        '__module__': __name__,
        'Meta': Meta
    }

    attrs.update({field['id']: get_field(field['type'], field['title']) for field in fields})

    return type(name, (models.Model,), attrs)


models_description = get_parsed_models()
for name, description in models_description.items():
    create_model(name, description['title'], description['fields'])