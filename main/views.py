from django.db.models import get_app, get_models
from django.views.generic import TemplateView
from rest_framework import routers, serializers, viewsets
from models import models_description

class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):

        context = super(IndexView, self).get_context_data(**kwargs)
        context['models'] = models_description
        return context

router = routers.DefaultRouter()


def create_views_for_model(model_class):
    class Serializer(serializers.ModelSerializer):
        class Meta:
            model = model_class

    class ViewSet(viewsets.ModelViewSet):
        queryset = model_class.objects.all()
        serializer_class = Serializer

    router.register(model_class.__name__, ViewSet)


app = get_app('main')
for model in get_models(app):
    create_views_for_model(model)