from django.contrib import admin
from django.db.models import get_app, get_models


def register_admin(model):
    admin.site.register(model)

app = get_app('main')
for model in get_models(app):
    register_admin(model)