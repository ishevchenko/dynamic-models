# coding=utf-8
import json
import datetime
from django.db.models import get_model
from django.test import TestCase, Client
from main.admin import register_admin
import models


class ModelsCreationTestCase(TestCase):
    def test_parses_YAML(self):
        YAML = u'''
users:
    title: Пользователи
    fields:
        - {id: name, title: Имя, type: char}
        - {id: paycheck, title: Зарплата, type: int}
        - {id: date_joined, title: Дата поступления на работу, type: date}
rooms:
    title: Комнаты
    fields:
        - {id: department, title: Отдел, type: char}
        - {id: spots, title: Вместимость, type: int}'''
        parsed_models = {
            'users': {
                'title': u'Пользователи',
                'fields': [
                    {'id': 'name', 'title': u'Имя', 'type': 'char'},
                    {'id': 'paycheck', 'title': u'Зарплата', 'type': 'int'},
                    {'id': 'date_joined', 'title': u'Дата поступления на работу', 'type': 'date'}
                ]
            },
            'rooms': {
                'title': u'Комнаты',
                'fields': [
                    {'id': 'department', 'title': u'Отдел', 'type': 'char'},
                    {'id': 'spots', 'title': u'Вместимость', 'type': 'int'}
                ]
            },
        }
        self.assertDictEqual(models.parse_models(YAML), parsed_models)

    def test_creates_fields(self):
        self.assertTrue(isinstance(models.get_field('int', 'integer'), models.models.IntegerField))
        self.assertTrue(isinstance(models.get_field('char', 'char'), models.models.TextField))
        self.assertTrue(isinstance(models.get_field('date', 'date'), models.models.DateField))

    def test_creates_model(self):
        model = models.create_model('users', u'Пользователи', [
            {'id': 'name', 'title': u'Имя', 'type': 'char'},
            {'id': 'paycheck', 'title': u'Зарплата', 'type': 'int'},
            {'id': 'date_joined', 'title': u'Дата поступления на работу', 'type': 'date'}
        ])
        self.assertTrue(issubclass(model, models.models.Model))
        field_names = [field.name for field in model._meta.fields]
        self.assertIn('name', field_names)
        self.assertIn('paycheck', field_names)
        self.assertIn('date_joined', field_names)

    def test_registers_admin(self):
        class SomeModel(models.models.Model):
            pass

        register_admin(SomeModel)
        from django.contrib import admin

        self.assertIn(SomeModel, admin.site._registry)


class ViewTestCase(TestCase):
    def setUp(self):
        self.user_model = get_model('main', 'users')
        self.user1 = self.user_model.objects.create(name='foo', paycheck=10, date_joined='2014-09-11')
        self.user2 = self.user_model.objects.create(name='bar', paycheck=100, date_joined='2014-09-10')

    def test_lists_items(self):
        c = Client()
        response = c.get('/users/')
        self.assertEqual(response.status_code, 200)
        users = json.loads(response.content)
        self.assertEqual(len(users), 2)
        self.assertEqual(users[0]['name'], 'foo')
        self.assertEqual(users[1]['name'], 'bar')

    def test_adds_item(self):
        c = Client()
        response = c.post('/users/', data=json.dumps({'name': 'baz', 'paycheck': 200, 'date_joined': '2014-09-09'}),
                          content_type='application/json')
        self.assertEqual(response.status_code, 201)
        user = self.user_model.objects.get(name='baz')
        self.assertEqual(user.paycheck, 200)
        self.assertEqual(user.date_joined, datetime.date(2014, 9, 9))

    def test_does_not_add_not_full_item(self):
        c = Client()
        response = c.post('/users/', data=json.dumps({'name': 'baz', 'paycheck': 200}),
                          content_type='application/json')
        self.assertEqual(response.status_code, 400)
        users = self.user_model.objects.filter(name='baz')
        self.assertEqual(len(users), 0)

    def test_does_not_add_invalid_item(self):
        c = Client()
        response = c.post('/users/', data=json.dumps({'name': 'baz', 'paycheck': 200, 'date_joined': 'invalid value'}),
                          content_type='application/json')
        self.assertEqual(response.status_code, 400)
        users = self.user_model.objects.filter(name='baz')
        self.assertEqual(len(users), 0)

    def test_edits_item(self):
        c = Client()
        response = c.put('/users/%s/' % self.user1.id, data=json.dumps({'name': 'baz', 'paycheck': 200, 'date_joined': '2014-09-09'}),
                         content_type='application/json')
        self.assertEqual(response.status_code, 200)
        user = self.user_model.objects.get(id=self.user1.id)
        self.assertEqual(user.name, 'baz')
        self.assertEqual(user.paycheck, 200)
        self.assertEqual(user.date_joined, datetime.date(2014, 9, 9))

    def test_does_not_edit_with_invalid_date(self):
        c = Client()
        response = c.put('/users/%s/' % self.user1.id, data=json.dumps({'name': 'baz', 'paycheck': 200,
                                                                        'date_joined': 'invalid value'}),
                         content_type='application/json')
        self.assertEqual(response.status_code, 400)
        user = self.user_model.objects.get(id=self.user1.id)
        self.assertEqual(user.name, 'foo')